package com.simple_pojo;

public class PlayerData {
    private String playerName;
    private String playerTeam;
    private int jerNo;

    public String getPlayerName() {
        return playerName;
    }
    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }
    public String getPlayerTeam() {
        return playerTeam;
    }
    public void setPlayerTeam(String playerTeam) {
        this.playerTeam = playerTeam;
    }
    public int getJerNo() {
        return jerNo;
    }
    public void setJerNo(int jerNo) {
        this.jerNo = jerNo;
    }
    
}

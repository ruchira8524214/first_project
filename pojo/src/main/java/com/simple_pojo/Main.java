package com.simple_pojo;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.println("Player Data");

        PlayerData obj = new PlayerData();

        System.out.print("Enter Player Name : ");
        String playerName = sc.nextLine();
        System.out.print("Enter Player Country : ");
        String playerTeam = sc.nextLine();
        System.out.print("Enter Jersy Number : ");
        int jerNo = sc.nextInt();
        obj.setPlayerName(playerName);
        obj.setPlayerTeam(playerTeam);
        obj.setJerNo(jerNo);

        System.out.println("Player Name : " + obj.getPlayerName());
        System.out.println("Player Country : " + obj.getPlayerTeam());
        System.out.println("Player Jersy Number : " + obj.getJerNo());
    }
}